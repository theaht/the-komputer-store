// Getting all the elements we need from the document.
const elWorkSection = document.getElementById('work-section');
const elBankBalance = document.getElementById('bank-balance');
const elGetLoan = document.getElementById('get-loan');
const elLoanForm = document.getElementById('loan-form');
const elDisplayLoan = document.getElementById('display-loan');
const elLoanAmount = document.getElementById('loan-amount');
const elApplyLoan = document.getElementById('apply-loan');
const elLoanBalance = document.getElementById('loan-balance')
const elWorkButton = document.getElementById('work-button');
const elPayButton = document.getElementById('pay-button');
const elPayBalance = document.getElementById('pay-balance');
const elLaptops = document.getElementById('laptop-selection');
const elLaptopFeatures = document.getElementById('laptop-features');
const elLaptopName = document.getElementById('selected-laptop-name');
const elLaptopDescription = document.getElementById('selected-laptop-description');
const elLaptopPhoto = document.getElementById('selected-laptop-photo');
const elLaptopSelected = document.getElementById('selected-laptop-row');
const elLaptopPrize = document.getElementById('selected-laptop-price');
const elBuyLaptopButton = document.getElementById('buy-laptop-button');


//Hiding the elements we don't want to display before requested by the user
elLoanForm.style.display = "none";
elLaptopSelected.style.display = "none";
elDisplayLoan.style.display = "none";

let repayLoan;
let currentBankBalance = 5000;
let currentWorkBalance = 0;
let currentLoanBalance = 0;
let hasLoan = false;

//The array containing the available laptops
const availableLaptops = [
    {
        name: 'MacBook Pro',
        id: 1,
        price: 15000,
        description: 'First-class performance in a professional packaging!',
        photo: 'https://dam.which.co.uk/c5d4a8e0-16b2-4116-84a2-2e304a6705fc.jpg',
        features: [
            '15in Retina', 'Touch Bar', '2.8GHz Intel Core i7 Quad Core', '16GB RAM', '256GB SSD'
        ]
    },
    {
        name: 'MacBook Air',
        id: 2,
        price: 13000,
        description: 'Fast, thin and light!',
        photo: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqBipW2gVUW8yE8DkDXfmCndzp_WFcnT0sOQ&usqp=CAU',
        features: [
            '13.3in Retina', 'Intel Core i5', '1.6 GHz', 'Intel UHD Graphics 617', '128 GB'
        ]
    },
    {
        name: 'Acer Aspire 5',
        id: 3,
        price: 7990,
        description: 'Powerful laptop with large storage space and 10 hours of battery life!',
        photo: 'https://images-na.ssl-images-amazon.com/images/I/71sesDsw95L._AC_SL1500_.jpg',
        features: [
            '15.6 inches Full HD IPS Display', 'Vega 3 Graphics', '4GB DDR4', '128GB SSD', 'Backlit Keyboard', 'Windows 10 in S Mode'
        ]
    },
    {
        name: 'HP EliteBook 840 G7',
        id: 4,
        price: 13490,
        description: '\n' +
            'Has extensive safety features, long battery life and a durable aluminum case that is perfect for professionals on the go.',
        photo: 'https://support.hp.com/doc-images/493/c06680937.png',
        features: [
            '10th Gen Intel Core i7-10610U', '16GB RAM', '512GB PCIe SSD', 'Backlit Keyboard', 'Fingerprint Reader', 'WiFi 6', 'USB-C', 'Windows 10'
        ]
    },
    {
        name: 'Lenovo ThinkPad E15',
        id: 5,
        price: 8990,
        description: 'Delivers first-class performance day after day, and has all the functionality you need!',
        photo: 'https://static.bhphoto.com/images/images2500x2500/1583426808_1549118.jpg',
        features: [
            '15.6 inches FHD Full HD', 'Intel 10txh Quad Core i5-10210U', '16GB DDR4 RAM', '512GB PCIe SSD', 'Windows 10 Pro'
        ]
    }

];

/*
* Adds the list of available laptops to the select list.
* */
for(const laptop of availableLaptops){
    const elLaptopOption = document.createElement('option');
    elLaptopOption.value = laptop.id;
    elLaptopOption.innerText = laptop.name;
    elLaptops.appendChild(elLaptopOption);
}

//Showing the current balance.
elBankBalance.innerText = currentBankBalance + ' kr.';

/*
* Event listener on the "Get a loan"-button.
* If the button is clicked and the user already has a loan, a message will pop up.
* If the user does not have a loan, the form for applying a loan is displayed.
 */
elGetLoan.addEventListener('click', function () {
    if(hasLoan){
        window.alert("You already have a loan");
    }
    else {
        elLoanForm.style.display = "block";
    }
});

/*
* EventListener on the  "Apply loan button". If the value of the input-field is not a number,
* a message is showed to the user.
* If the wanted amount is more than double of the current balance, the user is told that the wanted
* amount is too high.
* Otherwise the the loan is a approved and the current balance increases, the amount
* of loan is displayed.
* A button for repaying loan is added to the work section. The user is told the loan is approved and
* the loan form is hidden.
* */
elApplyLoan.addEventListener('click', function () {
    const wantedLoanAmount = Number(elLoanAmount.value)
    elLoanAmount.value = '';
    if(!isNaN(wantedLoanAmount) ){
        if(wantedLoanAmount >= currentBankBalance*2){
            window.alert("Wanted loan amount is too high");
            elLoanForm.style.display = 'none';
            return;
        }
        currentLoanBalance = wantedLoanAmount;
        elLoanBalance.innerText = currentLoanBalance + ' kr.';

        currentBankBalance += currentLoanBalance;
        elBankBalance.innerText = currentBankBalance + ' kr.';
        hasLoan = true;

        repayLoan = document.createElement('button.buttons');
        repayLoan.setAttribute('class', 'buttons');
        repayLoan.id = 'repay-loan-button';
        repayLoan.innerText = 'Repay Loan';
        listenOnRepayLoanButton(repayLoan);
        elWorkSection.appendChild(repayLoan);

        window.alert("The loan is approved");
        elLoanForm.style.display = "none";
        elDisplayLoan.style.display = "block";
    }
    else {
        window.alert("Please type in a valid amount");
    }
});

/*
* EventListener on the "Work"-button.
* If the button is clicked, 100 kr is added to the work balance.
* */
elWorkButton.addEventListener('click', function () {
    currentWorkBalance += 100;
    elPayBalance.innerText = currentWorkBalance + ' kr.';

});

/*
* Eventlistener on the pay-button. If the button is clicked and the user does not have a loan,
* the amount on the work-account is transferred to the the bank-account, and the work-balance is set to 0.
* If the user has a loan. 10% of the work-balance is payed to the loan, the rest is transferred to the
* bank-account. If 10% is more than what is left to pay to the loan, the loan is set to 0 and the rest is
* transferred to the bank-account
* */
elPayButton.addEventListener('click', function (){
    if(hasLoan) {
        const payToLoan = currentWorkBalance * 0.1;
        const rest = payLoan(payToLoan);
        if (rest !== 0) {
            transferToBankAccount(currentWorkBalance * 0.9 + rest);
        } else {
            transferToBankAccount(currentWorkBalance * 0.9);
        }
    }
    else{
        transferToBankAccount(currentWorkBalance);
    }
    currentWorkBalance = 0;
    elPayBalance.innerText = currentWorkBalance + ' kr.';
});

/*
* Eventlistener on the repay loan button,
* */
function listenOnRepayLoanButton(repayLoanButton){
    repayLoanButton.addEventListener('click', function (){
        const rest = payLoan(currentWorkBalance);
        if(rest !== 0){
            transferToBankAccount(rest);

        }
        currentWorkBalance = 0;
        elPayBalance.innerText = currentWorkBalance + ' kr.';
    });
}

/*
* Function for paying loan. If the amount is higher or equals to the loan amount, the loan is deleted.
* If not, the amount is subtracted from the loan.
* */
function payLoan(amount){
    if(amount >= currentLoanBalance) {
        const rest = amount - currentLoanBalance;
        elWorkSection.removeChild(repayLoan);
        hasLoan = false;
        currentLoanBalance = 0;
        elLoanBalance.innerText = '';
        elDisplayLoan.style.display = "none";
        return rest;
    }

    currentLoanBalance -= amount;
    elLoanBalance.innerText = currentLoanBalance + ' kr.';
    return 0;

}

/*
* Increases the bank balance with the given amount.
* */
function transferToBankAccount(amount){
    currentBankBalance += amount;
    elBankBalance.innerText = currentBankBalance + ' kr.';
}

/*
* Eventlistener on the select laptops. If the user chooses a different option, the information about
* the current laptop is updated.
* */
elLaptops.addEventListener('change', function (){
    const value = Number(this.value);
    elLaptopFeatures.innerHTML = '';


    if(value === -1){
        elLaptopSelected.style.display = 'none';
        return;
    }
    elLaptopSelected.style.display = 'block';
    const selectedLaptop = availableLaptops.find(function (laptop){
        return laptop.id === value;
    });
    for(const feature of selectedLaptop.features){

        const elFeature = document.createElement('li');
        elFeature.innerText = feature;
        elLaptopFeatures.appendChild(elFeature);
    }
    elLaptopName.innerText = selectedLaptop.name;
    elLaptopDescription.innerText = selectedLaptop.description;
    elLaptopPrize.innerText = 'Price: ' + selectedLaptop.price + ' kr.';
    elLaptopPhoto.src = selectedLaptop.photo;

})

/*
* Eventlistener on the buy laptop button.
* If the user can afford the computer, the current bank balance decreases with the price
* and the user is told he is the owner of the new computer.
* If the user can not afford the computer, a message is displayed.
* */
elBuyLaptopButton.addEventListener('click', function (){
    const currentLaptop = availableLaptops.find(function (laptop){
        return laptop.id === Number(elLaptops.value);
    });

    if(currentLaptop.price <= currentBankBalance){

        currentBankBalance -= currentLaptop.price;
        elBankBalance.innerText = currentBankBalance + ' kr.';
        window.alert("You are now the owner of the new laptop");
    }
    else {
        window.alert("You can not afford this laptop");
    }
})


